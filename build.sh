#!/usr/bin/env bash

set -ex

cargo update
cargo run -- ./odd_jobs.json > ./out/README.md
